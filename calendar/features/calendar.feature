  Feature: Calendar Feature
	If I am a logged in user
	Then I should be able to use calendar

  Scenario: I am on the current month 
	Given I am on the calendar page
	Then I should be on the current month

  Scenario: I can swipe to different months
	Given I am logged in the application
	Then I should see the month calendar
	And I can swipe right
	And I can swipe left

  Scenario: I can navigate to different months
	Given I am on the calendar view
	Then I can click next month
	And I can click previous month