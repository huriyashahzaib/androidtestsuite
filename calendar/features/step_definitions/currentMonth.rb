require 'calabash-android/calabash_steps'

Given (/^I am on the calendar view$/) do 
	loginToApp()
end

Then (/^I can click next month$/) do
	wait_for_element_exists("android.view.View id:'#{'month_view'}'")
	nextMonthDate = nextMonth()
	tap_when_element_exists("andriod.widget.ImageButton id:'#{'next'}'")
	wait_for_element_exists("android.view.View id:'#{nextMonthDate}'")
end

And (/^I can click previous month$/) do
	wait_for_element_exists("android.view.View id:'#{'month_view'}'")
	prevMonthDate = prevMonth()
	tap_when_element_exists("andriod.widget.ImageButton id:'#{'prev'}'")
	tap_when_element_exists("andriod.widget.ImageButton id:'#{'prev'}'")
	wait_for_element_exists("android.view.View id:'#{preMonthDate}'")	


