require 'calabash-android/calabash_steps'

Given (/^I am logged in the application$/) do 
	loginToApp()
end

Then (/^I should see the month calendar$/) do
	wait_for_element_exists("android.view.View id:'#{'month_view'}'")
end

And (/^I can swipe right$/) do
	nextDate = nextMonth()
	perform_action('swipe',"right")
	wait_for_element_exists("android.view.View id:'#{nextDate}'")
end

And (/^I can swipe left$/) do
	prevDate = prevMonth()
	perform_action("swipe","left")
	perform_action("swipe","left")
	wait_for_element_exists("android.view.View id:'#{prevDate}'")
end
