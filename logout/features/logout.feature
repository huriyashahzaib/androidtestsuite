  Feature: Logout Feature
	As a logged in user
	I should be able to logout when I want

  Scenario Outline: Logging out of the application.
	Given I am logged in
	When I click signout menu option 
	Then I click <option> on logout popup
	Then I am logged out and I see title <title>

  Examples:
	| option | title |
  	| "button1" | "AutomateMe" |
	| "button2" | "HomActivity" |