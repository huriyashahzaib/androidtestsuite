require 'calabash-android/calabash_steps'

Given (/^I am logged in$/) do 
	loginToApp()
end

Then (/^I click signout menu option$/) do 
	tap_when_element_exists("android.widget.ImageView  {contentDescription LIKE[c] '#{'More options'}'}")
	tap_when_element_exists("android.widget.TextView {text CONTAINS[c] '#{'Sign out'}'}")
end

Then (/^I click "([^\"]*)" on logout popup$/) do |button|
	wait_for_text("Are you sure you want to logout?")
	tap_when_element_exists("android.widget.Button id:'#{button}'")
end

Then (/^I am logged out and I see title "([^\"]*)"$/) do |title|
	wait_for_text(title)
end

