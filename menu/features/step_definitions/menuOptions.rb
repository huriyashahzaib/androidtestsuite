require 'calabash-android/calabash_steps'

Given (/^I am on the calendar page$/) do 
	loginToApp()
end

When (/^I click on menu icon$/) do
	tap_when_element_exists("android.widget.ImageButton contentDescription:'#{'Open navigation drawer'}'")
end

Then (/^the menu item is "([^\"]*)"$/) do |menu|
	tap_when_element_exists("android.widget.CheckedTextView text:'#{menu}'")
end



