require 'calabash-android/calabash_steps'

Given (/^I have menu opened$/) do 
	loginToApp()
end

When (/^I press back button$/) do
	press_back_button
end

Then (/^menu should close$/) do
	wait_for_text_to_disappear("Import")
end



