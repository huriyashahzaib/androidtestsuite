  Feature: Menu
	If I am a logged in user
	Then I should be able to see a menu on the left

  Scenario: Show logged in user
	Given I am logged in 
	Then I should see on menu user "Automateme@tscodelab.com" 

  Scenario: Back button closes the menu
	Given I have menu opened
	When I press back button
	Then menu should close

  Scenario Outline: Show menu items
	Given I am on the calendar page
	When I click on menu icon
	Then the menu item is <menu>

  Examples:
	|menu|
	|"Import"|
	|"Gallery"|
	|"Slideshow"|
	|"Tools"|
	|"Share"|
	|"Send"|