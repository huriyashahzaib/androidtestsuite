require 'calabash-android/calabash_steps'

Given (/^I have opened the application$/) do 
	wait_for_text("AutomateMe")
end

When (/^I donot enter email and touch "([^\"]*)"$/) do |buttonName|
	tap_when_element_exists("android.widget.Button id:'#{buttonName}'")
end

Then (/^I see error message "([^\"]*)"$/) do |error|
	sleep(5)
	wait_for_text(error)
end
