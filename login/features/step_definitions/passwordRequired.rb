require 'calabash-android/calabash_steps'

Given (/^I am on the main screen$/) do 
	wait_for_text("AutomateMe")
end

When (/^I tap "([^\"]*)" input field$/) do |email|
	tap_when_element_exists("android.widget.EditText id:'#{email}'")
end

Then (/^I type in the input field "([^\"]*)"$/) do |username|
	wait_for_keyboard()
	keyboard_enter_text username
end

Then (/^I press the button "([^\"]*)"$/) do |buttonName|
	tap_when_element_exists("android.widget.Button id:'#{buttonName}'")
end

Then (/^I see validation error "([^\"]*)"$/) do |error|
	begin
		sleep(5)
		wait_for_text(error)
	rescue
		print "Validation did not occur"
	end
end