require 'calabash-android/calabash_steps'

Given (/^I want to login$/) do 
	wait_for_text("AutomateMe")
end

When (/^I touch "([^\"]*)" input field$/) do |email|
	tap_when_element_exists("android.widget.EditText id:'#{email}'")
end

Then (/^I use the keyboard to type "([^\"]*)"$/) do |username|
	wait_for_keyboard()
	keyboard_enter_text username
end

Then (/^I touch the "([^\"]*)" input field$/) do |passfield|
	tap_when_element_exists("android.widget.EditText id:'#{passfield}'")
end

Then (/^I use keyboard to type "([^\"]*)"$/) do |password|
	wait_for_keyboard()
	keyboard_enter_text password
end

Then (/^I touch "([^\"]*)"$/) do |buttonName|
	tap_when_element_exists("android.widget.Button id:'#{buttonName}'")
end

Then (/^I see text on screen "([^\"]*)"$/) do |title|
	sleep(5)
	wait_for_text(title)
end

