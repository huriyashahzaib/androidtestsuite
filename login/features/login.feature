  Feature: Login Feature
	In order to use the system,
	one must be able to login

  Scenario: Checking that email field is required.
	Given I have opened the application
	When I donot enter email and touch "email_sign_in_button"
	Then I see error message "This field is required"

  Scenario: Checking that password field is required. 
	Given I am on the main screen
	When I tap "email" input field
	Then I type in the input field "automateme@tscodelab.com"
	Then I press the button "email_sign_in_button"
	Then I see validation error "This field is required"

  Scenario Outline: Checking password length
	Given I want to login
	When I touch "email" input field
   	Then I use the keyboard to type <email>
	Then I touch the "password" input field
    	Then I use the keyboard to type <password>
    	Then I touch "email_sign_in_button"
	Then I see text on screen <text>

  Examples:
	| email | password | text |
	| "automateme@tscodelab.com"| "tigerspike" | "HomActivity" |
	| "automateme@codelab.com"| "bla" | "This password is too short"|