  Feature: Remember Me
	If I select remember me then my login info should be remembered 

  Scenario Outline: Remember Me
    Given I am on login page
    And I toggle Remember Me <toggle>
    And I login
    Then I logout 
    Then I should see <email>
    
  Examples:
    |toggle|email|
    |"On"|"automateme@tscodelab.com"|
    |"Off"|""|