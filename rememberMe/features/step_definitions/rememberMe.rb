require 'calabash-android/calabash_steps'

Given (/^I am on login page$/) do
	wait_for_text("AutomateMe")
end

And (/^And I toggle Remember Me "([^\"]*)"$/) do |toggle,email|
    tap_when_element_exists("android.widget.ToggleButton id:'#{'toggleButton'}'")
    wait_for_text(toggle)
    loginToApp()
    logout()
    wait_for_text(email)
end