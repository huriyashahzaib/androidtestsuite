require 'date'

def getCurrentDate
    current_date = DateTime.now.strftime("%d %m %Y")
    return current_date
end

def nextMonth
    (Date.today>>1).strftime("%d %m %Y")
end

def prevMonth
    (Date.today<<1).strftime("%d %m %Y")
end
