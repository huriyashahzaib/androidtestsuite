def loginToApp
	wait_for_text("AutomateMe")
	tap_when_element_exists("android.widget.EditText id:'#{'email'}'")
	wait_for_keyboard()
	keyboard_enter_text "automateme@tscodelab.com"
	tap_when_element_exists("android.widget.EditText id:'#{'password'}'")
	wait_for_keyboard()
	keyboard_enter_text "tigerspike"
	tap_when_element_exists("android.widget.Button id:'#{'email_sign_in_button'}'")
	sleep(5)
	wait_for_text("HomActivity")
end

def logout
	tap_when_element_exists("android.widget.ImageView  {contentDescription LIKE[c] '#{'More options'}'}")
	tap_when_element_exists("android.widget.TextView {text CONTAINS[c] '#{'Sign out'}'}")
    wait_for_text("Are you sure you want to logout?")
	tap_when_element_exists("android.widget.Button id:'#{'button1'}'")
end