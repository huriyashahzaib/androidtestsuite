require 'calabash-android/calabash_steps'

Given (/^I click on "([^\"]*)"$/) do |forgetPassword|
	tap_when_element_exists(forgetPassword)
end

And (/^I am on "([^\"]*)" page) do |pageTitle|
	wait_for_text(pageTitle)
end

Then (/^I see security "([^\"]*)") do |question|
	wait_for_element_exists(question)
end

Then (/^I enter "([^\"]*)" in "([^\"]*)" ) do |answer,questionField|
	enter_text("android.widget.EditText id:'#{questionField}'",answer)
end

Then (/^I should see "([^\"]*)") do |result|
	wait_for_text(result)
end

