  Feature: Forget Password
	If as a user I forgot my password
	I should be able to recover it

  Scenario Outline: Forget password recovery
	Given I click on "fogot_pwd_text"
	And I am on "Forgot Password" page
	Then I see security <question>
	Then I enter <answer> in "security_q1_text"
	Then I should see <result>

  Examples:
	|question|answer|result
	|"security_q1_text"|""|"Should not be empty"
	|"security_q2_text"|"answer"|"Answer you have provided is wrong!"